import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import Cross from "../../public/images/news/Cross";
import styles from "../styles/topnews.module.scss";
import Info from "../../public/images/news/Info";
import { signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import AppDispatcher from "../redux/dispatchers/appDispatcher";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
import { auth } from "../firebase";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 270,
  height: 100,
  bgcolor: "background.paper",
  border: "1px solid #1e2e4a",
  boxShadow: 24,
  borderRadius: 3,
  p: 4,
};

export default function Warning() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  function onClickRedirection() {
    const provider = new GoogleAuthProvider();
    signInWithPopup(auth, provider)
      .then((res) => {
        AppDispatcher.LogInHolder(true, res.user.photoURL);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  return (
    <div>
      <Button onClick={handleOpen}>
        <div className={styles.anchor_container}>
          <a onClick={handleOpen}>{t.readmore} &#10095;</a>
        </div>
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            <div className={styles.warning_container}>
              <Info />
              <div className={styles.warning_title_container}>
                {t.signinreadmore}
                <div className={styles.modal_link} onClick={onClickRedirection}>
                  <a>{t.googleredirect}</a>
                </div>
              </div>
            </div>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
}

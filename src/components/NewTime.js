import React from "react";
import Moment from "react-moment";
import style from "../styles/topnews.module.scss";
import Time from "../../public/images/news/Time";
function NewTime(props) {
  return (
    <div className={style.show_time}>
      <div className={style.time_clock}>
        <Time />
      </div>
      <div className={style.updated_time}>
        <Moment fromNow>{props.publishedAt}</Moment>
      </div>
    </div>
  );
}

export default NewTime;

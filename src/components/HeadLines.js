import React from "react";
import style from "../styles/headline.module.scss";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
function HeadLines(props) {
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  const publishDate = props.publishedAt.split(/[T,Z]+/);
  publishDate.pop();
  return (
    <div className={style.headline_container}>
      <a>
        <div className={style.image_container}>
          {/*eslint-disable */}
          <img src={props.image} alt="image" />
          {/* eslint-enable */}
        </div>
      </a>

      <div className={style.headline_content_container}>
        <div className={style.headline_title_container}>{props.title}</div>
        <div className={style.sources_container}>
          <div>{`${t.author} : ${props.author}`}</div>
          <div>{`${t.newssource} : ${props.source}`}</div>
          <div>{`${t.date} : ${publishDate[0]}`}</div>
          <div>{`${t.time} : ${publishDate[1]}`}</div>
        </div>
      </div>
    </div>
  );
}
export default HeadLines;

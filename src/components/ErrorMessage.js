import React from "react";
import style from "../styles/topnews.module.scss";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
function ErrorMessage() {
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  return (
    <div className={style.bookmark_container}>
      <div className={style.error_box}>
        <div>{t.warning}</div>
      </div>
    </div>
  );
}

export default ErrorMessage;

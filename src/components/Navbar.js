import React, { useState } from "react";
import style from "../styles/navbar.module.scss";
import { useSelector } from "react-redux";

import Language from "./Language";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  signOut,
} from "firebase/auth";
import AppDispatcher from "../redux/dispatchers/appDispatcher";
import { auth } from "../firebase";
import Link from "next/link";
function Navbar() {
  const router = useRouter();

  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  console.log(t);
  const getApi = useSelector((state) => state.getApidata);
  const userLogin = getApi.logInData[0];

  function inSignInHandler() {
    const provider = new GoogleAuthProvider();
    signInWithPopup(auth, provider)
      .then((res) => {
        AppDispatcher.LogInHolder(true, res.user.photoURL);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function inSignOuthandler() {
    AppDispatcher.LogInHolder(false, "");

    const auth = getAuth();
    signOut(auth)
      .then((res) => {})
      .catch((error) => {
        console.log(error);
      });
  }
  return (
    <header className={style.header}>
      <div className={style.logo}>
        <Link href="/news/latest-news">{t.logo}</Link>
      </div>
      <div className={style.search_content}>
        <nav className={style.navigation}>
          <ul>
            <li>
              <Link href="/news/latest-news">{t.latestnews}</Link>
            </li>
            <li>
              <Link href="/news/top-headlines">{t.topheadlines}</Link>
            </li>
            <li>
              <Link href="/news/search-news">{t.searchnews}</Link>
            </li>
            <li>
              <Link href="/news/book-mark">{t.bookmarks}</Link>
            </li>
            <li>
              <Link href="/news/sources">{t.sources}</Link>
            </li>
            <li>
              <button
                className={style.toggle_button}
                onClick={userLogin.login ? inSignOuthandler : inSignInHandler}
              >
                {userLogin.login ? `${t.signout}` : `${t.signin}`}
              </button>
            </li>
            <li>
              {userLogin.login ? (
                <div className={style.user_image}>
                  {/*eslint-disable */}
                  <img src={userLogin.username} />
                  {/* eslint-enable */}
                </div>
              ) : null}
            </li>
            <li>
              <Language />
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}

export default Navbar;

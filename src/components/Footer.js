import React from "react";
import { Button } from "./Button";
import Link from "next/link";
import enUS from "locales/en-US";
import ar from "locales/ar";
import { useRouter } from "next/router";
import es from "locales/es";
function Footer() {
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  return (
    <div className="footer-container">
      <section className="footer-subscription">
        <p className="footer-subscription-heading">{t.join}</p>
        <p className="footer-subscription-text">{t.unsubscribe}</p>
        <div className="input-areas">
          <form>
            <input
              className="footer-input"
              name="email"
              type="email"
              placeholder={`${t.mailplaceholder}`}
            />
            <Button buttonStyle="btn--outline">{t.subscribe}</Button>
          </form>
        </div>
      </section>
      <div className="footer-links">
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>{t.aboutus}</h2>
            <Link href="/news/latest-news">{t.work}</Link>
            <Link href="/news/latest-news">{t.testimonials}</Link>
            <Link href="/news/latest-news">{t.career}</Link>
            <Link href="/news/latest-news">{t.investors}</Link>
            <Link href="/news/latest-news">{t.terms}</Link>
          </div>
          <div className="footer-link-items">
            <h2>{t.contactus}</h2>
            <Link href="/news/latest-news">{t.contact}</Link>
            <Link href="/news/latest-news">{t.Support}</Link>
            <Link href="/news/latest-news">{t.destination}</Link>
            <Link href="/news/latest-news">{t.sponsor}</Link>
          </div>
        </div>
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>{t.videos}</h2>
            <Link href="/news/latest-news">{t.submitvideo}</Link>
            <Link href="/news/latest-news">{t.ambassador}</Link>
            <Link href="/news/latest-news">{t.agency}</Link>
            <Link href="/news/latest-news">{t.influencer}</Link>
          </div>
          <div className="footer-link-items">
            <h2>{t.socialmedia}</h2>
            <Link href="/news/latest-news">{t.instagram}</Link>
            <Link href="/news/latest-news">{t.facebook}</Link>
            <Link href="/news/latest-news">{t.youtube}</Link>
            <Link href="/news/latest-news">{t.twitter}</Link>
          </div>
        </div>
      </div>
      <section className="social-media">
        <div className="social-media-wrap">
          <small className="website-rights">{`${t.copuright} © 2022`}</small>
        </div>
      </section>
    </div>
  );
}

export default Footer;

import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Link from "next/link";
import { useRouter } from "next/router";
export default function Language() {
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  let router = useRouter();
  console.log(router);
  return (
    <div>
      <div style={{ minWidth: 80, marginLeft: "-40px" }}>
        <FormControl fullWidth>
          <InputLabel
            sx={{
              color: "#fff",
              fontSize: "11px",
              fontWeight: "600",
              margin: "-16px 5px 4px 16px",
            }}
            id="demo-simple-select-label"
          >
            lang
          </InputLabel>
          <Select
            sx={{
              color: "white",
              height: "31px",
              width: "78px",
              fontSize: "10px",
              margin: "-6px -13px 0px 17px",
              border: "1px solid #a5b0c7",
              //   padding: "4.5px 9px",
            }}
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={age}
            label="Age"
            onChange={handleChange}
          >
            <MenuItem
              sx={{
                height: "30px",
                fontSize: "13px",
                padding: "5.5px 5px",
                border: "none",
              }}
              value={10}
            >
              <Link href={router.asPath} locale="en-US" passHref>
                English
              </Link>
            </MenuItem>

            <MenuItem
              sx={{
                height: "30px",
                fontSize: "13px",
                padding: "5.5px 8px",
                border: "none",
              }}
              value={20}
            >
              <Link href={router.asPath} locale="ar" passHref>
                Arabic
              </Link>
            </MenuItem>
            <MenuItem
              sx={{
                height: "30px",
                fontSize: "13px",
                padding: "5.5px 8px",
                border: "none",
              }}
              value={30}
            >
              <Link href={router.asPath} locale="es" passHref>
                Spanish
              </Link>
            </MenuItem>
          </Select>
        </FormControl>
      </div>
    </div>
  );
}

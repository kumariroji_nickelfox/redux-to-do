import React from "react";
import Search from "public/images/news/Search";
import style from "../styles/newscard.module.scss";
function SearchNewNews() {
  return (
    <div>
      <div className={style.search_container}>
        <input type="text" placeholder="Search here" />
        <div className={style.search_icon}>
          <Search />
        </div>
      </div>
    </div>
  );
}
export default SearchNewNews;

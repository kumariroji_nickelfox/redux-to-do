import React from "react";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
function HeroSection() {
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  return (
    <div className="hero-container">
      <video autoPlay loop muted>
        <source src="/video-2.mp4" />
      </video>
      <div className="header__text-box">
        <h1 className="heading-primary">
          <span className="heading-primary--main">{t.newsworldwide}</span>
          <span className="heading-primary--sub">{t.news}</span>
        </h1>
        <a href="#" className="btn btn--white btn--animated">
          {t.button}
        </a>
      </div>
    </div>
  );
}

export default HeroSection;

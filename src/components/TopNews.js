import React, { useState } from "react";
import Link from "next/link";
import style from "../styles/topnews.module.scss";
import Bookmark from "../../public/images/news/Bookmark";
import AppDispatcher from "../redux/dispatchers/appDispatcher";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
import Delete from "public/images/news/Delete";
import Warning from "./Warning";
import NewTime from "./NewTime";

import { useSelector } from "react-redux";
function TopNews(props) {
  const getApi = useSelector((state) => state.getApidata);
  const getApiData = getApi.bookmark;
  const apiDataArr = getApiData.map((ele) => {
    return JSON.stringify(ele);
  });
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  const query = router.pathname;
  function getBookmarkData() {
    if (apiDataArr.includes(JSON.stringify(props.data)) == false) {
      AppDispatcher.AddBookMark(props.data);
    }
  }
  function deleteBookmarkData() {
    AppDispatcher.DeleteBookMark(props.data);
  }
  return (
    <div className={style.news_main_container}>
      <a>
        <div className={style.news_image_container}>
          {/*eslint-disable */}
          <img src={props.image} alt="image" />
          {/* eslint-enable */}
        </div>
      </a>

      <div className={style.news_content_container}>
        <div className={style.source_news}>{props.source}</div>
        <div className={style.news_heading}>{props.title}</div>
        <div className={style.news_time_container}>
          <NewTime publishedAt={props.publishedAt} />
        </div>
        <div className={style.news_content}>{props.description}</div>
        <div className={style.link_container}>
          {getApi.logInData[0].username == "" ? (
            <Warning />
          ) : (
            <div className={style.anchor_container}>
              <Link href={props.links} passHref>
                <a target="_blank">{t.readmore} &#10095;</a>
              </Link>
            </div>
          )}

          {query == "/news/latest-news" || query == "/news/search-news" ? (
            <div onClick={getBookmarkData} className={style.bookmark}>
              <Bookmark markButton={props.markButton} />
            </div>
          ) : query == "/news/book-mark" ? (
            <div onClick={deleteBookmarkData} className={style.bookmark}>
              <Delete />
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default TopNews;

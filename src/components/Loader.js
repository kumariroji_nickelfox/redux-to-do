import React from "react";
import { BeatLoader } from "react-spinners";
import { css } from "@emotion/react";
import style from "../styles/topnews.module.scss";

function Loader() {
  return (
    <div className={style.loader_container}>
      <BeatLoader size={40} color="#6b2731" loading />
    </div>
  );
}

export default Loader;

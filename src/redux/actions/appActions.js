const Actions = {
  LOGIN: "SET_USER_STATE",
  SET_TOKEN: "SET_AUTH_TOKEN",
  LOGOUT: "SET_USER_LOGOUT",
};
export const addTodo = (data) => {
  return {
    type: "ADD_TODO",
    payload: {
      id: new Date().getTime().toString(),
      data: data,
      checked: false,
    },
  };
};
export const deleteTodo = (id) => {
  return {
    type: "DELETE_TODO",
    payload: {
      id: id,
    },
  };
};
export const updateTodo = (data, id) => {
  return {
    type: "UPDATE_TODO",
    payload: {
      id: id,
      data: data,
    },
  };
};
export const checkedTodo = (id) => {
  return {
    type: "CHECKED_TODO",
    payload: {
      id: id,
    },
  };
};
export const getApiData = (data) => {
  return {
    type: "GET_API_DATA",
    payload: {
      data: data,
    },
  };
};
export const addBookMark = (data) => {
  return {
    type: "ADD_BOOKMARK",
    payload: {
      data: data,
      marked: false,
    },
  };
};
export const deleteBookMark = (data) => {
  return {
    type: "DELETE_BOOKMARK",
    payload: {
      data: data,
    },
  };
};
export const logInHolder = (data, username) => {
  return {
    type: "LOGIN_HOLDER",
    payload: {
      data: data,
      username: username,
    },
  };
};

export const sourcesData = (data) => {
  return {
    type: "SOURCES_DATA",
    payload: {
      data: data,
    },
  };
};
export default Actions;

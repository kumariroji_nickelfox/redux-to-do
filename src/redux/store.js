import { createStore, combineReducers } from "redux";
import AppReducer from "./reducers/appReducer";
import todoReducer from "./reducers/todolist";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import getApidata from "./reducers/apidata";
const persistConfig = {
  key: "root",
  storage,
};
const AllReducer = {
  app: AppReducer,
  todoReducer: todoReducer,
  getApidata: getApidata,
};

const rootReducer = combineReducers(AllReducer);
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer);
export const persistor = persistStore(store);
export default store;

// App wide dispatcher

import Stores from "../store";
import Actions from "../actions/appActions";
import {
  addTodo,
  deleteTodo,
  updateTodo,
  checkedTodo,
  getApiData,
  addBookMark,
  deleteBookMark,
  logInHolder,
  sourcesData,
} from "../actions/appActions";

const AppDispatcher = {
  setUserLoggedIn: (data) => {
    Stores.dispatch({ type: Actions.SET_TOKEN, data: data.tokens });
    Stores.dispatch({ type: Actions.LOGIN, data: data.user });
  },
  setUserLoggedOut: () => {
    Stores.dispatch({ type: Actions.LOGOUT });
  },
  updateUserTokens: (data) => {
    Stores.dispatch({ type: Actions.SET_TOKEN, data });
  },
  updateUserInfo: (data) => {
    Stores.dispatch({ type: Actions.LOGIN, data });
  },
  AddTodo: (data) => {
    Stores.dispatch(addTodo(data));
  },
  DeleteTodo: (id) => {
    Stores.dispatch(deleteTodo(id));
  },
  UpdateTodo: (data, id) => {
    Stores.dispatch(updateTodo(data, id));
  },
  CheckedTodo: (id) => {
    Stores.dispatch(checkedTodo(id));
  },
  GetApiData: (data) => {
    Stores.dispatch(getApiData(data));
  },
  AddBookMark: (data) => {
    Stores.dispatch(addBookMark(data));
  },
  DeleteBookMark: (data) => {
    Stores.dispatch(deleteBookMark(data));
  },
  LogInHolder: (data, username) => {
    Stores.dispatch(logInHolder(data, username));
  },
  SourcesData: (data) => {
    Stores.dispatch(sourcesData(data));
  },
};
export default AppDispatcher;

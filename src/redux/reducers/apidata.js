const initialState = {
  getList: [],
  bookmark: [],
  markList: [],
  logInData: [{ username: "", login: false }],
  sources: [],
};
const getApidata = (state = initialState, action) => {
  switch (action.type) {
    case "GET_API_DATA":
      const data = action.payload.data;

      return {
        ...state,
        getList: [data],
        markList: [{ marked: false }],
      };
    case "ADD_BOOKMARK":
      const markdata = action.payload.data;

      return {
        ...state,
        bookmark: [...state.bookmark, markdata],
        markList: [...state.bookmark, markdata, { marked: true }],
      };
    case "DELETE_BOOKMARK":
      const bookmarkData = action.payload.data;
      const markArr = state.bookmark.filter((data) => {
        return data != bookmarkData;
      });
      return {
        ...state,
        bookmark: markArr,
      };
    case "LOGIN_HOLDER":
      const logindata = action.payload.data;
      const username = action.payload.username;
      return {
        ...state,
        logInData: [{ username: username, login: logindata }],
      };
    case "SOURCES_DATA":
      const sourcesdata = action.payload.data;
      return {
        ...state,
        sources: [sourcesdata],
      };

    default:
      return state;
  }
};
export default getApidata;

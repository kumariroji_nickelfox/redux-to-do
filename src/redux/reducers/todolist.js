const initialState = {
  list: [],
};

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TODO":
      const { id, data } = action.payload;
      return {
        ...state,
        list: [
          {
            id: id,
            data: data,
            checked: false,
          },
          ...state.list,
        ],
      };
    case "DELETE_TODO":
      const todoId = action.payload.id;
      const listFilter = state.list.filter((ele) => {
        return ele.id != todoId;
      });
      return { ...state, list: listFilter };
    case "UPDATE_TODO":
      const updateTodoId = action.payload.id;
      const editData = action.payload.data;
      const updatedTodoList = state.list.map((ele) => {
        if (ele.id == updateTodoId) {
          return {
            id: updateTodoId,
            data: editData,
          };
        } else {
          return ele;
        }
      });
      return {
        ...state,
        list: updatedTodoList,
      };
    case "CHECKED_TODO":
      const checkedId = action.payload.id;

      let checkedData = "";
      state.list.map((ele) => {
        if (ele.id == checkedId) {
          checkedData = ele;
        }
      });
      checkedData.checked = true;
      const checkedFilter = state.list.filter((ele) => {
        return ele.id != checkedId;
      });
      checkedFilter.push(checkedData);

      return {
        ...state,
        list: checkedFilter,
      };
    default:
      return state;
  }
};
export default todoReducer;

import * as React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { useRouter } from "next/router";
import { getSession } from "@session/cookie";
import Image from "next/image";
import ImagePath from "../../public/images/placeholders/onboardingng.jpg";
import Navbar from "@components/Navbar";
import Footer from "@components/Footer";
export default function PublicLayout({ children }) {
  // const { replace } = useRouter();
  // const session = getSession("user-token");

  // React.useEffect(() => {
  //   if (session) {
  //     replace("/user/dashboard", "/user/dashboard");
  //   }
  // }, [session]);

  return (
    // <Box sx={{ flexGrow: 1 }}>
    //   <Grid container spacing={2}>
    //     <Grid item xs={7}>
    //       <Image src={ImagePath} alt="PlaceholderImage" />
    //     </Grid>
    //     <Grid item xs={5}>
    //       {children}
    //     </Grid>
    //   </Grid>
    // </Box>
    <>
      <Navbar />
      {children}
      <Footer />
    </>
  );
}

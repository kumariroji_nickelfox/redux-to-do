import React, { useState } from "react";
import AppDispatcher from "../../redux/dispatchers/appDispatcher";
import { useSelector } from "react-redux";
import style from "../../styles/todo.module.scss";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Delete from "public/images/todo/Delete";
import Edit from "public/images/todo/Edit";

import Arrow from "public/images/todo/Arrow";
function TODO() {
  const [addTodo, setAddTodo] = useState("");
  const [emptyList, setEmptyList] = useState(false);
  const [editTodo, setEditTodo] = useState("");
  const [number, setNumber] = useState(-1);
  const [editActive, setEditActive] = useState(false);
  const newState = useSelector((state) => state.todoReducer);

  const list = newState.list;
  function onClickHandler(e) {
    setAddTodo(e.target.value);
  }
  const warningText = <p style={{ color: "red" }}>Field is empty</p>;
  function onSubmitHandler(event) {
    event.preventDefault();
    if (addTodo.length == 0) {
      setEmptyList(true);
    } else {
      AppDispatcher.AddTodo(addTodo);
      setEmptyList(false);
    }
    setAddTodo("");
  }
  function onEditHandler(num) {
    setNumber(num);
    setEditActive(true);
    setEditTodo("");
  }
  function onEditChangeHandler(e) {
    setEditTodo(e.target.value);
  }

  return (
    <form onSubmit={onSubmitHandler}>
      <div className={style.todo_container}>
        <h1>To Do App</h1>
        <div className={style.todo_input_container}>
          <div className={style.input_container}>
            <input type="text" value={addTodo} onChange={onClickHandler} />
          </div>
          <button type="submit" className={style.add_button}>
            Add
          </button>
        </div>
        <div>{emptyList == true ? warningText : null}</div>
        <div className={style.checkbox_container}>
          {list.map((item) => (
            <>
              <FormGroup key={item.id} className={style.inner_container}>
                <div className={style.todo_full_container}>
                  <div>
                    <FormControlLabel
                      onChange={() => {
                        AppDispatcher.CheckedTodo(item.id);
                      }}
                      checked={item.checked}
                      control={<Checkbox />}
                      label={item.data}
                    />
                  </div>
                  <div className={style.svg_container}>
                    <div onClick={() => onEditHandler(item.id)}>
                      {!item.checked ? <Edit /> : null}
                    </div>
                    <div
                      onClick={() => {
                        AppDispatcher.DeleteTodo(item.id);
                      }}
                    >
                      <Delete />
                    </div>
                  </div>
                </div>

                {editActive && item.id == number ? (
                  <div className={style.edit_input_box}>
                    <input
                      type="text"
                      value={editTodo}
                      onChange={onEditChangeHandler}
                    />
                    <div
                      className={style.arrow_container}
                      onClick={() => {
                        AppDispatcher.UpdateTodo(editTodo, item.id);
                        setEditActive(false);
                      }}
                    >
                      <Arrow />
                    </div>
                  </div>
                ) : null}
              </FormGroup>
            </>
          ))}
        </div>
      </div>
    </form>
  );
}
export default TODO;

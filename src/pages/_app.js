import React from "react";
import "../styles/globals.css";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useRouter } from "next/router";
import { Provider as ReduxProvider } from "react-redux";
import PublicLayout from "@layouts/publicLayout";
import PrivateLayout from "@layouts/privateLayout";
import { defaultTheme } from "@themes/defaultTheme";
import { PersistGate } from "redux-persist/integration/react";

import store from "@redux/store";
import { persistor } from "@redux/store";

function MyApp({ Component, pageProps }) {
  const currentTheme = createTheme(defaultTheme);
  const path = useRouter();

  const isPublic = path.asPath.includes("/auth/");
  const isPrivate = path.asPath.includes("/user/");

  const Wrapper = isPublic
    ? PublicLayout
    : isPrivate
    ? PrivateLayout
    : PublicLayout;

  return (
    <ReduxProvider store={store}>
      <PersistGate persistor={persistor}>
        <ThemeProvider theme={currentTheme}>
          <Wrapper>
            <Component {...pageProps} />
          </Wrapper>
        </ThemeProvider>
      </PersistGate>
    </ReduxProvider>
  );
}

export default MyApp;

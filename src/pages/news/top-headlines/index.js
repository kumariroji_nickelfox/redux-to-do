import React from "react";
import HeadLines from "@components/HeadLines";
import { useSelector } from "react-redux";
import ErrorMessage from "@components/ErrorMessage";
function TopHeadlines() {
  const getApi = useSelector((state) => state.getApidata);
  if (getApi.getList.length == 0 || getApi.getList[0].status == "error") {
    return <ErrorMessage />;
  } else {
    const getdata = getApi.getList[0].articles;
    return (
      <div>
        <div>
          {getdata.map((ele, id) => (
            <HeadLines
              key={id}
              links={ele.url}
              image={ele.urlToImage}
              title={ele.title}
              author={ele.author}
              publishedAt={ele.publishedAt}
              source={ele.source.name}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default TopHeadlines;

import React, { useState } from "react";
import { useSelector } from "react-redux";
import TopNews from "@components/TopNews";
import AppDispatcher from "../../../redux/dispatchers/appDispatcher";
import ErrorMessage from "@components/ErrorMessage";
import HeroSection from "@components/HeroSection";
export const getStaticProps = async () => {
  const res = await fetch("https://newsapi.org/v2/top-headlines?country=in", {
    headers: {
      // Authorization: "500bfadb7c714b45854772c0361cf427",
      Authorization: process.env.NEXT_PUBLIC_API_KEY,
    },
  });

  const data = await res.json();
  return {
    props: {
      data,
    },
  };
};
function Latestnews({ data }) {
  const [toggle, setToggle] = useState(true);
  function dispatchApiData(dataS) {
    AppDispatcher.GetApiData(dataS);
    setToggle(false);
  }
  {
    toggle ? dispatchApiData(data) : null;
  }
  const getApi = useSelector((state) => state.getApidata);
  console.log(getApi);

  if (getApi.getList.length == 0) {
    return <ErrorMessage />;
  } else {
    const getdata = getApi.getList[0].articles;
    let toggleButton = false;
    const bookmarkData = getApi.bookmark.map((data) => {
      return JSON.stringify(data);
    });
    return (
      <div>
        <HeroSection />
        {getdata?.map((ele, id) => {
          if (bookmarkData.includes(JSON.stringify(ele))) {
            toggleButton = true;
          } else {
            toggleButton = false;
          }
          return (
            <TopNews
              key={id}
              data={ele}
              id={id}
              links={ele.url}
              image={ele.urlToImage}
              title={ele.title}
              content={ele.content}
              description={ele.description}
              publishedAt={ele.publishedAt}
              source={ele.source.name}
              markButton={toggleButton}
            />
          );
        })}
      </div>
    );
  }
}

export default Latestnews;

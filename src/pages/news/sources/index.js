import React, { useState } from "react";
import style from "../../../styles/sources.module.scss";
import { useRouter } from "next/router";
import AppDispatcher from "../../../redux/dispatchers/appDispatcher";
import { useSelector } from "react-redux";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";

import ErrorMessage from "@components/ErrorMessage";
export const getStaticProps = async () => {
  const res = await fetch(
    "https://newsapi.org/v2/top-headlines/sources?&pageSize=5",
    {
      headers: {
        // Authorization: "78f9f018c2a74cad8f3fe4dc66dd6c2c",
        Authorization: process.env.NEXT_PUBLIC_API_KEY,
      },
    }
  );
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
};
function Source({ data }) {
  const [toggle, setToggle] = useState(true);

  // console.log(getData);
  // console.log(getApi);
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  function onClickHandler(e) {
    router.push("/news/sources");
    window.open(e);
  }

  function dispatchSourceData(dataS) {
    AppDispatcher.SourcesData(dataS);
    setToggle(false);
  }
  {
    toggle ? dispatchSourceData(data) : null;
  }
  const getApi = useSelector((state) => state.getApidata);
  console.log(getApi);
  const getData = getApi.sources[0].sources;

  console.log(getData);
  return (
    <div>
      {getApi.sources.length == 0 || getApi.sources[0].status == "error" ? (
        <ErrorMessage />
      ) : (
        <div className={style.sources_container}>
          <h1>{t.newssource}</h1>
          <div className={style.sources_wrapper}>
            {getData.map((ele, id) => (
              <>
                <div
                  key={id}
                  className={style.source_link_container}
                  onClick={() => onClickHandler(`/news/sources/${ele.id}`)}
                >
                  <a>{ele.name}</a>
                </div>
              </>
            ))}{" "}
          </div>
        </div>
      )}
    </div>
  );
}

export default Source;

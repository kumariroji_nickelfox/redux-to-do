import React, { useState } from "react";
import Search from "public/images/news/Search";
import style from "../../../styles/search.module.scss";
import TopNews from "@components/TopNews";
export const getStaticPaths = async () => {
  const res = await fetch(
    "https://newsapi.org/v2/top-headlines/sources?&pageSize=5",
    {
      headers: {
        Authorization: process.env.NEXT_PUBLIC_API_KEY,
      },
    }
  );
  const data = await res.json();
  console.log(data);
  const paths = data.sources.map((ele) => {
    return {
      params: { sourceId: ele.id.toString() },
    };
  });
  return {
    paths,
    fallback: false,
  };
};
export const getStaticProps = async (context) => {
  const id = context.params.sourceId;
  const res = await fetch(
    `https://newsapi.org/v2/top-headlines?sources=${id}`,
    {
      headers: {
        Authorization: process.env.NEXT_PUBLIC_API_KEY,
      },
    }
  );
  const data = await res.json();
  return {
    props: {
      data,
    },
  };
};
function NewSources({ data }) {
  const [searchData, setSearchData] = useState("");
  const [userData, setUserData] = useState(data.articles);
  function onSubmitHandler(e) {
    e.preventDefault();
  }
  function onChangeHandler(e) {
    setSearchData(e.target.value);
    let arr = searchData.toLowerCase();
    const userSearchData = data.articles.filter((ele) => {
      return ele.title.toLowerCase().includes(arr);
    });
    setUserData(userSearchData);
    if (searchData == "") {
      setUserData(data.articles);
    }
  }
  function onClickHandler() {}
  console.log(data);
  return (
    <div>
      <form onSubmit={onSubmitHandler}>
        <div className={style.search_wrapper}>
          <div className={style.search_container}>
            <input
              type="text"
              placeholder="Search here"
              onChange={onChangeHandler}
              value={searchData}
            />
            <div onClick={onClickHandler} className={style.search_icon}>
              <Search />
            </div>
          </div>
        </div>
      </form>
      {userData?.map((ele) => (
        <TopNews
          key={ele}
          data={ele}
          links={ele.url}
          image={ele.urlToImage}
          title={ele.title}
          content={ele.content}
          description={ele.description}
          publishedAt={ele.publishedAt}
          source={ele.source.name}
        />
      ))}
    </div>
  );
}

export default NewSources;

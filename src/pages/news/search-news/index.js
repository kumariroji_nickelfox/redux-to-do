import React, { useState } from "react";
import Search from "public/images/news/Search";
import style from "../../../styles/search.module.scss";
import { useSelector } from "react-redux";
import TopNews from "@components/TopNews";
import enUS from "locales/en-US";
import ar from "locales/ar";
import es from "locales/es";
import { useRouter } from "next/router";
import ErrorMessage from "@components/ErrorMessage";
function SearchNews() {
  const [searchData, setSearchData] = useState("");
  const [error, setError] = useState(false);
  const [value, setValue] = useState("");
  const router = useRouter();
  const t = router.locale === "en-US" ? enUS : router.locale === "ar" ? ar : es;
  const getApi = useSelector((state) => state.getApidata);
  if (getApi.getList.length == 0 || getApi.getList[0].status == "error") {
    function onErrorSubmitHandler(e) {
      e.preventDefault();
      setError(true);
      setSearchData("");
    }

    function onErrorCall(e) {
      setSearchData(e.target.value);
    }
    return (
      <div style={{ height: "300vh" }}>
        <form onSubmit={onErrorSubmitHandler}>
          <div className={style.search_wrapper}>
            <div className={style.search_container}>
              <input
                type="text"
                placeholder={`${t.searchbox}`}
                onChange={onErrorCall}
                value={searchData}
              />
              <div onClick={onErrorCall} className={style.search_icon}>
                <Search />
              </div>
            </div>
          </div>
        </form>
        {error ? <ErrorMessage /> : null}
      </div>
    );
  } else {
    const getdata = getApi.getList[0].articles;

    function onClickHandler() {}
    function onSubmitHandler(e) {
      e.preventDefault();
      let arr = searchData.toLowerCase();
      const userSearchData = getdata.filter((ele) => {
        return ele.title.toLowerCase().includes(arr);
      });
      setValue(userSearchData);
      setSearchData("");
    }

    function onChangeHandler(e) {
      setSearchData(e.target.value);
    }
    return (
      <div style={{ height: "300vh" }}>
        <form onSubmit={onSubmitHandler}>
          <div className={style.search_wrapper}>
            <div className={style.search_container}>
              <input
                type="text"
                placeholder={`${t.searchbox}`}
                onChange={onChangeHandler}
                value={searchData}
              />
              <div onClick={onClickHandler} className={style.search_icon}>
                <Search />
              </div>
            </div>
          </div>
        </form>

        {value != ""
          ? value.map((ele, id) => (
              <TopNews
                key={id}
                data={ele}
                links={ele.url}
                image={ele.urlToImage}
                title={ele.title}
                content={ele.content}
                publishedAt={ele.publishedAt}
                description={ele.description}
                source={ele.source.name}
              />
            ))
          : null}
      </div>
    );
  }
}

export default SearchNews;

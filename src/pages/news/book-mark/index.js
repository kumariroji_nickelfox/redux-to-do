import React from "react";
import Language from "@components/language";
import { useSelector } from "react-redux";
import TopNews from "../../../components/TopNews";
import PageBookmark from "@components/PageBookmark";
import MainHeader from "@components/MainHeader";
import Cross from "public/images/news/Cross";
import Hamburger from "public/images/news/Hamburger";
import Sidebar from "@components/Sidebar";
import style from "../../../styles/topnews.module.scss";
function Bookmark() {
  const getApi = useSelector((state) => state.getApidata);
  const getApiData = getApi.bookmark;
  const dataArr = [];
  getApiData.map((ele) => {
    if (dataArr.includes(ele) == false) {
      dataArr.push(ele);
    } else {
      return;
    }
  });
  return (
    <div>
      {dataArr.length == 0 ? (
        <PageBookmark />
      ) : (
        <div className={style.bookmark_container}>
          {dataArr.map((data, id) => (
            <TopNews
              key={id}
              data={data}
              links={data.url}
              image={data.urlToImage}
              title={data.title}
              content={data.content}
              description={data.description}
              publishedAt={data.publishedAt}
              source={data.source.name}
            />
          ))}
        </div>
      )}
    </div>
  );
}

export default Bookmark;

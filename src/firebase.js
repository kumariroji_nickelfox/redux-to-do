// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCKs2asTKBm61Evwa-g2ZLd5dcsTdgW34A",
  authDomain: "news-web-dda4f.firebaseapp.com",
  projectId: "news-web-dda4f",
  storageBucket: "news-web-dda4f.appspot.com",
  messagingSenderId: "540113011180",
  appId: "1:540113011180:web:434ba763d4c701e6cc00c9",
};
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

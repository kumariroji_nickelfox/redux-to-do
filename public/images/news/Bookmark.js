import React from "react";
import style from "../../../src/styles/topnews.module.scss";
import { useSelector } from "react-redux";
function Bookmark(props) {
  // const getApi = useSelector((state) => state.getApidata);
  // const bookMarkData = getApi.bookmark;
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={props.markButton ? style.bookmarked : style.book_unmarked}
      viewBox="0 0 20 20"
      fill="currentColor"
    >
      <path d="M5 4a2 2 0 012-2h6a2 2 0 012 2v14l-5-2.5L5 18V4z" />
    </svg>
  );
}

export default Bookmark;

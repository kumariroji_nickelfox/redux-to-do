import React from "react";

function Time() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      style={{
        height: "18px",
        width: "18px",
        color: "#1e2e4a",
      }}
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
      />
    </svg>
  );
}

export default Time;

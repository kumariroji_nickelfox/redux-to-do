require("dotenv").config({
  path: `environments/.env.${process.env.APP_ENV || "dev"}`,
});

module.exports = {
  reactStrictMode: true,
  env: process.env.APP_ENV,
};

module.exports = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/",
        permanent: true,
      },
    ];
  },
};
module.exports = {
  i18n: {
    locales: ["en-US", "ar", "es"],
    defaultLocale: "en-US",
  },
};
